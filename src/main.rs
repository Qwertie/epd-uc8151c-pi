
use rppal::gpio::Gpio;
use rppal::spi::{Bus, Mode, Segment, SlaveSelect, Spi};
use std::error::Error;
use std::thread;
use std::time::Duration;

extern crate image;
use image::{ImageFormat, GenericImageView}; 

mod constants;

//Pins
const PIN_RESET: u8 = 17;
const PIN_DC: u8 = 25;
const PIN_CS: u8 = 8;
const PIN_BUSY: u8 = 24;


struct Epd {
  reset_pin: rppal::gpio::OutputPin,
  dc_pin: rppal::gpio::OutputPin,
  cs_pin: rppal::gpio::OutputPin,
  busy_pin: rppal::gpio::InputPin,
  spi: rppal::spi::Spi,
  three_color: bool
}

impl Epd {
  fn reset(&mut self) {
    self.reset_pin.set_high();
    thread::sleep(Duration::from_millis(200));
    self.reset_pin.set_low();
    thread::sleep(Duration::from_millis(200));
    self.reset_pin.set_high();
    thread::sleep(Duration::from_millis(200));
  }

  fn epd_init(&mut self) {
    self.reset();

    self.send_command(constants::booster_soft_start);
    self.send_data(0x17);
    self.send_data(0x17);
    self.send_data(0x17);

    self.send_command(constants::power_setting);
    self.send_data(0x03);
    self.send_data(0x00);
    self.send_data(0x2B);
    self.send_data(0x2B);
    self.send_data(0x09);
   
    self.send_command(constants::power_on);
    println!("Waiting for power on");
    self.wait_for_idle();
    println!("Powered on");

    self.send_command(constants::panel_setting);
    if self.three_color {
      self.send_data(0xCF);
    } else {
      self.send_data(0xDF);
    }

    // Since our screen resolution is not in the presets of panel_setting we have to set resolution again
    self.send_command(constants::resolution_setting);
    self.send_data(0x98);
    self.send_data(0x01);
    self.send_data(0x28);
 
    self.send_command(constants::vcom_and_data_interval_setting);
    self.send_data(0xF7);
  }

  // Writes a white frame to the display and refreshes it
  fn clear_display(&mut self) {
    println!("start clear display");
    const EPD_WIDTH: u32 = 152;
    const EPD_HEIGHT: u32 = 296;

    // We set 8 pixels at a time
    let width = if EPD_WIDTH % 8 == 0 {
      EPD_WIDTH / 8 
    } else { 
      EPD_WIDTH / 8 + 1
    };
    let height = EPD_HEIGHT;
    
    self.send_command(constants::display_transmission_1);
    for _n in 0..height {
        for _i in 0..width {
          self.send_data(0xff);
        }
    }
    self.send_command(constants::display_transmission_2);
    for _n in 0..height {
      for _i in 0..width {
        self.send_data(0xFF);
      }
    }

    self.send_command(constants::display_refresh);
    self.wait_for_idle();
  }

  // There are 2 frame memory areas. In 3 color mode the first one is black/white and the second one is the third color
  // In the 2 color mode the first one is OLD and the second one is NEW. Not sure what OLD is useful for
  fn write_image(&mut self, image1: image::DynamicImage, image2: image::DynamicImage) {
    if self.three_color {
      // Write to the B/W image buffer
      self.send_command(constants::display_transmission_1);
      self.send_pixels(image1);
  
      // Write to the 3rd color (Red/Yellow)
      self.send_command(constants::display_transmission_2);
      self.send_pixels(image2);
    } else {
      // Write to the NEW data area
      self.send_command(constants::display_transmission_2);
      self.send_pixels(image1);
    }

  }

  fn send_pixels(&mut self, image: image::DynamicImage) {
    let mut bitcount: u8 = 0;
    let mut byte: u8 = 0;
    for pixel in image.pixels() {
      // If red value of pixel is 255 we treat it as a 1, else 0
      if (pixel.2).0[0] == 255 {
        byte = byte | (1 << 7 - bitcount);
      } else {
        byte = byte & !(1 << 7 - bitcount);
      }
      bitcount += 1;
      if bitcount == 8 {
        self.send_data(byte);
        bitcount = 0;
      }
    }
  }

  fn wait_for_idle(&mut self) {
    while self.busy_pin.is_low() {
      thread::sleep(Duration::from_millis(100));
    }    
  }

  fn send_command(&mut self, command: u8) {
    self.dc_pin.set_low();
    self.spi.write(&[command]);
  }

  fn send_data(&mut self, data: u8) {
    self.dc_pin.set_high();
    self.spi.write(&[data]);
  }

}


fn main() -> Result<(), Box<dyn Error>> {
  let gpio = Gpio::new()?;

  let mut epd = Epd {
    reset_pin: gpio.get(PIN_RESET)?.into_output(),
    dc_pin: gpio.get(PIN_DC)?.into_output(),
    cs_pin: gpio.get(PIN_CS)?.into_output(),
    busy_pin: gpio.get(PIN_BUSY)?.into_input(),
    spi: Spi::new(Bus::Spi0, SlaveSelect::Ss0, 10_000_000, Mode::Mode0)?,
    three_color: true
  };

  epd.epd_init();
  println!("Writing image to epd");
  epd.write_image(image::open("test1.bmp").unwrap(), image::open("test.bmp").unwrap());
  println!("Refreshing");
  epd.send_command(constants::display_refresh);
  epd.wait_for_idle();

  Ok(())
}
