// I have not tested all of these commands.
// I think some of them are not usable on the the arduino/rpi dev boards since there is no MISO line

pub const panel_setting: u8 = 0x00;
pub const power_setting: u8 = 0x01;
pub const power_off: u8 = 0x02;
pub const power_off_ss: u8 = 0x03;
pub const power_on: u8 = 0x04;
pub const power_on_measure: u8 = 0x05;
pub const booster_soft_start: u8 = 0x06;
pub const deep_sleep: u8 = 0x07;
pub const display_transmission_1: u8 = 0x10;
pub const data_stop: u8 = 0x11;
pub const display_refresh: u8 = 0x12;
pub const display_transmission_2: u8 = 0x13;
pub const pll_control: u8 = 0x30;
pub const temperature_sensor_calibration: u8 = 0x40;
pub const temperature_sensor_selection: u8 = 0x41;
pub const temperature_sensor_write: u8 = 0x42;
pub const temperature_sensor_read: u8 = 0x43;
pub const vcom_and_data_interval_setting: u8 = 0x50;
pub const low_power_detection: u8 = 0x51;
pub const tcon_setting: u8 = 0x60;
pub const resolution_setting: u8 = 0x61;
pub const revision: u8 = 0x70;
pub const get_status: u8 = 0x71;
pub const auto_measurement_vcom: u8 = 0x80;
pub const read_vcom_value: u8 = 0x81;
pub const read_vcom_dc_setting: u8 = 0x82;
pub const partial_window: u8 = 0x90;
pub const partial_in: u8 = 0x91;
pub const partial_out: u8 = 0x92;
pub const program_mode: u8 = 0xA0;
pub const active_programming: u8 = 0xA1;
pub const read_otp: u8 = 0xA2;
pub const cascade_setting: u8 = 0xE0;
pub const power_saving: u8 = 0xE3;
pub const force_temperature: u8 = 0xE5;
