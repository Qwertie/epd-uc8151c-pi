# Epd UC8151C Pi
A small rust library for using epaper displays using the UC8151C chip

This library allows you to display images on epaper displays sold by Buy Display. It includes the basic functionaily to turn on, write images, and refresh the display. More of the displays functionality will be usable later. 

Currently supports the 2.6" color and B/W displays. Adding suppor for the other sizes should be as simple as changing the value for resolution_setting

The datasheet for this chip can be found here: https://www.buydisplay.com/download/ic/UC8151C.pdf

![epd on raspberry pi](/uploads/279ec58cd3e33334cc9670cdb8413770/DSC01220.JPG)
